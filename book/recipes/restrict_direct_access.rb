list_path = "/etc/httpd/conf.d/restrict_direct_access.conf"

template "restrict_direct_access.conf" do
  path "/etc/httpd/conf.d/restrict_direct_access.conf"
  source "restrict_direct_access.conf.erb"
  owner "root"
  group "root"
  mode 0644
end
