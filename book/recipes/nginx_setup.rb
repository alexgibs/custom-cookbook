list_path = "/etc/apt/sources.list.d/nginx_ppa.list"

template "nginx_ppa.list" do
  path "/etc/apt/sources.list.d/nginx_ppa.list"
  source "nginx_ppa.list.erb"
  owner "root"
  group "root"
  mode 0644
end



bash "register_keys" do
  user "root"
  code <<-EOH
    apt-key adv --keyserver keyserver.ubuntu.com --recv-keys C300EE8C
    apt-get update
    apt-get -y -o Dpkg::Options::=--force-confdef install nginx 
  EOH
  only_if { ::File.exists?(list_path) }
end
