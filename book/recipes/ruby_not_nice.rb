script "ruby_not_nice" do
   interpreter "bash"
   user "root"
   code <<-EOH
	#!/bin/bash
	(lockdir=/tmp/notniceruby.lock
	if mkdir "$lockdir"
	then
	    echo >&2 "successfully acquired lock"
	    while [ 1 ]
	    do
	        for pid in `pgrep ruby`
	        do
	        if [ -z $pid ]
	        then
	            exit
	        else
	            niceval="$(ps -eo pid,nice | grep $pid | grep -v grep |  awk '{print $2}')"
	            if [ $niceval != "19" ]
	            then
	                denice=$(sudo renice 19 -p $pid)
	            fi
	        fi
	        done
	        sleep 1
	    done
	    trap 'rm -rf "$lockdir"' 0 
	else
	    echo >&2 "cannot acquire lock, giving up on $lockdir"
	    exit 0
	fi)&
   EOH
end