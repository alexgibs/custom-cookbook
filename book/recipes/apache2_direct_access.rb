list_path = "/etc/httpd/conf.d/nonhostheader.conf"

template "nginx_ppa.list" do
  path "/etc/httpd/conf.d/nonhostheader.conf"
  source "nonhostheader.conf.erb"
  owner "root"
  group "root"
  mode 0644
end



